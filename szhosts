#!/usr/bin/perl -w
use strict;
use warnings;

use Digest::MD5 qw(md5_hex);
use Fcntl qw(LOCK_SH LOCK_EX);
use Getopt::Long qw(GetOptions);
use Storable qw(freeze);
use Sys::Syslog qw( :standard :macros );
use Text::ParseWords qw(shellwords);

# TODO:
# - Support for IPv6...?
# - Perldocs, ezpkg?

my $app = "szhosts";
$0 =~ m#[^/]+$# and $app = $&;
$0 = $app;

$Storable::canonical = 1;

my(@inputs, @outputs, $domain, $foreground);
my($compdb, $comphash) = ({}, "");

########################################################################
## INPUT FORMATS

sub cleanhost {
	my $x = "@_";
	$x =~ tr#A-Z#a-z#;
	$x =~ tr#a-z0-9##cd;
	$x;
}
my %srctypes = (

	# ISC DHCP Daemon Leases (/var/db/dhcpd.leases)
	dhcpd => sub {
		my($ip, $host, $exp, $mac, %db);
		for $_ (@_) {
			m#^\s*lease\s+(\d+\.\d+\.\d+\.\d+)\s*{\s*$#i
			  and $ip = $1
			  and next;
			m#^\s*ends\s+\d+\s+(\d+/\d+/\d+\s+\d+:\d+:\d+)\s*;\s*$#i
			  and $exp = $1
			  and next;
			m#^\s*hardware\s+ethernet\s+(([0-9a-f]{2}:){5}[0-9a-f]{2})\s*;\s*$#i
			  and $mac = $1
			  and next;
			m#^\s*client-hostname\s+"(.*)"\s*;\s*#i
			  and $host = cleanhost($1)
			  and next;
			if(m#^\s*\}\s*$#i) {
				$host = cleanhost($host || ("anon" . substr(md5_hex($mac), 0, 12)));
				if(!$db{$host} or !$db{$host}{exp} or $db{$host}{exp} lt $exp) {
					$db{$host}{ip}        = $ip;
					$db{$host}{mac}{$mac} = 1;
					$db{$host}{exp}       = $exp;
				}
				$ip = $host = $exp = $mac = "";
			}
		}
		\%db;
	},

	# OpenVPN Version 2 Status File ( status / status-version 2)
	vpnstat => sub {
		my %db;
		for $_ (@_) {
			m#^\s*CLIENT_LIST,([^,]+),[^,]*,(\d+\.\d+\.\d+\.\d+)# or next;
			my $host = cleanhost($1);
			$db{$host}{ip}  = $2;
			$db{$host}{mac} = {};
		}
		\%db;
	},

	# OpenBSD hostname.wg* with wgaip lines annotated with #:host
	hostwg => sub {
		my %db;
		for $_ (@_) {
			m#^[^\#]*wgaip\s+((?:\d+\.){3}\d+).*?\#:(\S+)#
				and $db{cleanhost($2)}{ip} = $1;
		}
		\%db;
	},

	# Flat whitespace-delimited text file ("name ip mac")
	flat => sub {
		my %db;
		for $_ (@_) {
			my @w = ();
			for my $p (shellwords($_)) {
				$p =~ m/^#/ and last;
				push @w, $p;
			}
			my $host = cleanhost(shift(@w) or next);
			$db{$host}{ip} = (shift(@w) or next);
			$db{$host}{mac} = {
				map { $_ => 1 }
				grep { m#^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$# } @w};
		}
		\%db;
	});

########################################################################
## OUTPUT FORMATS

sub findlists {
	my $db   = shift();
	my $pref = shift();
	my $sub  = shift();
	my($block, $indent, @out);
	for $_ (@_) {
		if($block and m#^\s*\Q$pref\E\s*ENDLIST\b#) {
			$sub->( $db, $block,
				sub {
					push @out, grep { m#\S# } map {
						my $x = $_;
						chomp($x);
						"$indent$x$/"
					} @_;
				});
			$block = '';
		}
		$block or push @out, $_;
		m/^(\s*)\Q$pref\E\s*HOSTLIST\s+(.+)$/
		  and($indent, $block) = ($1, $2);
	}
	\@out;
}

sub mkouttype {
	my($pref, $sub) = @_;
	return sub {
		join("", @{findlists(shift(), $pref, $sub, @_)});
	};
}

sub maxlen {
	my $l = 0;
	for my $x (@_) { length($x) > $l and $l = length($x); }
	$l;
}

my %outtypes = (

	# /etc/hosts format
	hosts => mkouttype(
		"#:",
		sub {
			my($db, $block, $out) = @_;
			for my $h (sort keys %{$db}) {
				$db->{$h}{ip} =~ m#$block# or next;
				$out->("$db->{$h}{ip}\t$h.$domain\t$h");
			}
		}
	),

	# ISC DCHP Daemon Config (/etc/dhcpd.conf)
	dhcpd => mkouttype(
		"#:",
		sub {
			my($db, $block, $out) = @_;
			for my $h (sort keys %{$db}) {
				$db->{$h}{ip} =~ m#$block# or next;

				# http://serverfault.com/a/578798
				$out->( map {
						my $m = lc($_);
						$m =~ tr#0-9a-f##cd;
						"host $h-$m {"
						  . " option host-name \"$h\";"
						  . " hardware ethernet $_;"
						  . " fixed-address $db->{$h}{ip}; }"
					} sort keys %{$db->{$h}{mac}});
			}
		}
	),

	# NSD Forward Master Zone (/var/nsd/zones/master/zone.a)
	nsda => mkouttype(
		";-",
		sub {
			my($db, $block, $out) = @_;
			for my $h (sort keys %{$db}) {
				$db->{$h}{ip} =~ m#$block# or next;
				my $suff = ".$domain. IN A $db->{$h}{ip}";
				$out->("$h$suff", "*.$h$suff");
				for my $mac (sort keys %{$db->{$h}{mac}}) {
					my $m = lc($mac);
					$m =~ tr#0-9a-f##cd;
					$out->("mac$m$suff", "*.mac$m$suff");
				}
			}
		}
	),

	# NSD Reverse Master Zone (/var/nsd/zones/master/zone.ptr)
	nsdptr => mkouttype(
		";-",
		sub {
			my($db, $block, $out) = @_;
			for my $h (sort keys %{$db}) {
				$db->{$h}{ip} =~ m#$block# or next;
				my $revip = join(".", reverse split(m#\.#, $db->{$h}{ip}));
				$out->("$revip.in-addr.arpa. PTR $h.$domain.");
			}
		}
	),

	# Flat human-readable text file (like /etc/hostlist)
	flat => sub {
		my $db      = shift();
		my $maxhost = 2 + maxlen(keys %{$db});
		my $maxip   = 2 + maxlen(map { $db->{$_}{ip} } keys %{$db});
		join(   "",
			map {
				    substr($_ . (" " x $maxhost), 0, $maxhost)
				  . substr($db->{$_}{ip} . (" " x $maxip), 0, $maxip)
				  . join(" ", sort keys %{$db->{$_}{mac}})
				  . $/
			} sort keys %{$db});
	});

########################################################################
## OPTION PARSING

sub parseopt_core {
	my($types, $val) = @_;
	my($t, $f, $p) = split(":", $val, 2);
	$f =~ s#\|(.*)$## and $p = $1;
	$f =~ m#\S# or die("no file path specified in $val");
	my $tt = $types->{$t} or die("invalid format type $t");
	return {path => $f, proc => $tt, stat => "",
		text => "", post => $p};
}

sub parseopts {
	GetOptions(
		"f"   => \$foreground,
		"d=s" => \$domain,
		"i=s" => sub { push @inputs, parseopt_core(\%srctypes, $_[1]); },
		"o=s" => sub { push @outputs, parseopt_core(\%outtypes, $_[1]); });
	$domain  or die("no domain specified");
	@inputs  or die("no inputs specified");
	@outputs or die("no outputs specified");
}

########################################################################
## MAIN LOOP LOGIC

sub textscan {
	my($db, $mod) = @_;
	my $f = $db->{path};

	my $s = freeze([(stat($f))[0, 1, 7, 9, 10]]);
	my $statdirty = $s ne $db->{stat};
	$db->{dirty} or $statdirty or return;

	open(my $fh, "<", $f) or die($!);
	flock($fh, LOCK_SH) or die($!);
	my $t
	  = $statdirty
	  ? do { local $/; <$fh> }
	  : $db->{text};
	my $txtdirty
	  = $statdirty
	  ? $t ne $db->{text}
	  : 0;

	$statdirty and $db->{stat} = $s;
	if($txtdirty) {
		$db->{text}  = $t;
		$db->{dirty} = 1;
	}
	$db->{dirty} or return;

	$fh;
}

sub errtrap(&$) {
	my($sub, $pref) = @_;
	eval { $sub->(); };
	$@ or return;
	$@ =~ m#^SIG[A-Z]+\b# and die($@);
	warn("$pref: $@");
}

#@[mysys
sub mysys {
	use strict;

	my %allow = (0 => 1);
	ref $_[0] eq 'ARRAY'
	  and %allow = map { int($_) => 1 } @{shift()};

	my $r = system(@_);

	$r == -1 and die("@_: failed: $!");
	($r & 127)
	  and die(  "@_: died, signal "
		  . ($r & 127)
		  . ", coredump "
		  . (($r & 128) ? "present" : "absent"));
	$r = $r >> 8;
	$allow{$r}
	  or die(
		"@_: exited with value $r; expected " . join(", ", sort(keys %allow)));
	1;
}
#@]

sub mainloop {
	map { delete($_->{dirty}) } @inputs, @outputs;

	my $compdirty;
	for my $i (@inputs) {
		errtrap {
			textscan($i);
			$i->{dirty} or return;
			my $old = $compdirty ? "" : freeze($i->{db} // {});
			$i->{db} = $i->{proc}->(split(m#^#m, $i->{text}));
			if($old ne freeze($i->{db})) {
				syslog(LOG_INFO, "$i->{path}: changed");
				$compdirty or syslog(LOG_INFO, "composite invalidated");
				$compdirty = 1;
			}
		}
		$i->{path};
	}

	if($compdirty) {
		my(%db, %ipseen, %macseen);
		for my $i (@inputs) {
			for my $host (keys %{$i->{db}}) {
				$db{$host} and next;

				my $ip = $i->{db}{$host}{ip} or next;
				$ipseen{$ip} and next;
				$ipseen{$ip} = 1;

				$db{$host}{ip} = $ip;

				my %macs;
				if($i->{db}{$host}{mac}) {
					for my $mac (sort keys %{$i->{db}{$host}{mac}}) {
						$macseen{$mac} and next;
						$macseen{$mac} = 1;
						$macs{$mac}    = 1;
					}
				}
				$db{$host}{mac} = \%macs;
			}
		}
		my $nh = freeze(\%db);
		if($nh ne $comphash) {
			$compdb   = \%db;
			$comphash = $nh;
			map { $_->{dirty} = 1 } @outputs;
			syslog(LOG_INFO, "composite changed");
		}
	}

	my %posts;
	my %commits;
	for my $o (@outputs) {
		errtrap {
			my $fh = textscan($o);
			$o->{dirty} or return;
			my $t = $o->{proc}->(
				$compdb,
				split(m#^#m, $o->{text}));
			$t eq $o->{text} and return;
			syslog(LOG_INFO, "$o->{path}: changed");

			flock($fh, LOCK_EX) or die($!);
			my $n = "$o->{path}.new";
			open(my $nfh, ">", $n) or die($!);
			print $nfh $t;
			close($nfh);
			$commits{$n} = $o->{path};
			$o->{text} = $t;

			$o->{post} or return;
			$o->{post} =~ m#\S# or return;
			$posts{$o->{post}} = 1;
		}
		$o->{path};
	}

	if(%commits) {
		syslog(LOG_INFO, 'syncing ' . scalar(keys %commits) . ' file(s)...');
		system('sync');
		map { errtrap {
			rename($_, $commits{$_}) or die($!);
		} $_ } keys %commits;
	}

	for my $p (keys %posts) {
		errtrap {
			syslog(LOG_INFO, "$p: starting");
			mysys($p);
		}
		$p;
	}
}

########################################################################
## INITIALIZE

#@[lockfile
do {
	use strict;
	use Fcntl qw(:flock);

	my %locks = ();

	sub lockfile {
		for my $f (@_) {
			if($locks{$f}) {
				if(-f $f) {
					utime(undef, undef, $f);
					next;
				}
				close($locks{$f});
				delete $locks{$f};
			}
			my $fh;
			open($fh, ">>", $f) and flock($fh, LOCK_EX | LOCK_NB)
			  or die("$f: $!");
			$locks{$f} = $fh;
		}
	}
};
#@]
my $lockpath = "/tmp/$app.lock";

#@[daemon
sub daemon() {
	use strict;
	use IO::Handle;
	use POSIX "setsid";

	chdir("/");

	my($i, $o, $e);
	open($i, "<", "/dev/null") or die($!);
	open($o, ">", "/dev/null") or die($!);
	open($e, ">", "/dev/null") or die($!);
	STDIN->fdopen($i, 'r') or die $!;
	STDOUT->fdopen($o, 'w') or die $!;
	STDERR->fdopen($e, 'w') or die $!;

	my $p = fork();
	defined($p) or die("fork failed: $!");
	$p and exit(0);

	setsid();
	srand();
}
#@]

openlog($app, 'ndelay,pid,nowait,perror', LOG_DAEMON) or die($!);

map {
	$SIG{$_} = sub { die("SIG@_"); }
} qw(TERM INT HUP ALRM PIPE);
$SIG{__WARN__} = sub { syslog(LOG_WARNING, "@_"); };

eval {
	parseopts();
	lockfile($lockpath);
	$foreground or daemon();
	syslog(LOG_INFO, "started");
	while(1) {
		mainloop();
		lockfile($lockpath);
		sleep(1);
	}
};
$@ and syslog(LOG_ERR, $@);
select STDERR;
